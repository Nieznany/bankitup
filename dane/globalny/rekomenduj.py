import classes
import pandas as pd
import pickle

from dane.globalny.config import list_of_categories

sklepcsv = pd.read_csv('sklepy.csv')
nasz = pd.read_csv('../user/user.csv')

sklepy = [classes.Shop(i) for i in range(1,len(sklepcsv)+1)]
with open('klienty.pickle', 'rb') as fp:
    klienty = pickle.load(fp)

[grupa] = [x.group for x in klienty if x.id==nasz['ID'][0]]
klienci_z_grupy = [x.id for x in klienty if x.group==grupa]

for category in list_of_categories:
    rekomendacje = [(x.popularity_in_group(klienci_z_grupy), x.id) for x in sklepy if x.category == category]
    rek_sort = sorted(rekomendacje, key=lambda x: x[0])
    rek_sort.reverse()
    df = pd.DataFrame([item[1] for item in rek_sort[:3]])
    df.to_csv(category+'.csv')
