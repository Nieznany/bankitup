import dane.globalny.classes as classes
import pandas as pd
import pickle
from sklearn.cluster import KMeans
klienci = pd.read_csv('klienci.csv')

klienty = [classes.Customer(i) for i in range(1,len(klienci)+1)]
wektory = [cust.get_vector() for cust in klienty]
kmeans = KMeans(n_clusters=3)
kmeans.fit(wektory)
predykcje = kmeans.predict(wektory)
[klienty[i].upgrade_group(predykcje[i]) for i in range(len(predykcje))]

with open('klienty.pickle', 'wb') as fp:
    pickle.dump(klienty, fp)
with open('kmeans.pickle', 'wb') as fp:
    pickle.dump(kmeans, fp)
