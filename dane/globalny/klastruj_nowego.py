import pickle
import pandas as pd
import os
import dane.globalny.classes as classes

CSV_USER = os.path.join(os.path.dirname(os.path.realpath('.')), 'user', 'user.csv')
klient = pd.read_csv(CSV_USER)
with open('klienty.pickle', 'rb') as fp:
    klienty = pickle.load(fp)
with open('kmeans.pickle', 'rb') as fp:
    kmeans = pickle.load(fp)

print(klient['ID'][0])
nowy = classes.Customer(klient['ID'][0])
nowy.upgrade_group(kmeans.predict(nowy)) #  TODO
klienty.append(classes.Customer(klient['ID'][0]))

with open('klienty.pickle', 'wb') as fp:
    pickle.dump(klienty, fp)