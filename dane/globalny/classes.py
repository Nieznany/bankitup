import pandas as pd
import numpy as np
import logging
from datetime import datetime
import os

CATEGORIES = ['gastronomia', 'ogolnospozywczy', 'odziezowy', 'uslugi', 'rozrywka']

CSV_TRANSAKCJE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'transakcje.csv')
CSV_SKLEPY = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'sklepy.csv')
CSV_KLIENCI = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'klienci.csv')
CSV_USER_TRANSAKCJE = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
                                   'user', 'transakcje_usera.csv')


class Shop:
    def __init__(self, sklep_id):
        self.id = sklep_id
        self.df_sklepy = pd.read_csv(CSV_SKLEPY)
        self.df_transakcje_all = pd.read_csv(CSV_TRANSAKCJE)
        self.df_transakcje = self.df_transakcje_all.loc[self.df_transakcje_all['ID sklepu'] == self.id]

        row_df_sklepy = self.df_sklepy.loc[self.df_sklepy['ID'] == self.id]
        self.category = row_df_sklepy['kategoria'].tolist()[0]
        self.geo = [row_df_sklepy['Wys. geogr.'].tolist()[0],
                    row_df_sklepy['Szer. geogr.'].tolist()[0]]

    def mean_price(self):
            try:
                mean = np.mean(self.df_transakcje['kwota'].tolist())
            except Exception as e:
                mean = None
                logging.warning('It seems that nothing was bought in store {} \n {}'.format(str(self.id),
                                                                                            e))
            return mean

    def popularity_in_group(self, list_of_customers_ids):
        shops_same_category = self.df_sklepy.loc[self.df_sklepy['kategoria'] == self.category]['ID'].tolist()
        df_trans_in_customers = self.df_transakcje_all[self.df_transakcje_all['ID klienta'].isin(list_of_customers_ids)]
        df_trans_in_cust_cat = df_trans_in_customers[df_trans_in_customers['ID sklepu'].isin(shops_same_category)]
        popularity = len(df_trans_in_customers.loc[df_trans_in_customers['ID sklepu'] == self.id])/len(df_trans_in_cust_cat)
        return popularity


class Customer:
    def __init__(self, id1):
        self.id = id1
        self.group = None
        self.customers_df = pd.read_csv(CSV_KLIENCI)
        self.transactions_of_user = pd.read_csv(CSV_TRANSAKCJE)
        self.transactions_of_user = self.transactions_of_user.loc[self.transactions_of_user['ID klienta'] == self.id]
        self.row_customer_df = self.customers_df.loc[self.customers_df['ID'] == self.id]
        self.age = self.row_customer_df['Wiek'].tolist()[0]
        self.sex = self.row_customer_df['Plec'].tolist()[0]
        self.salary = self.row_customer_df['kwota'].tolist()[0]

        self.df_shops = pd.read_csv(CSV_SKLEPY)

    def upgrade_group(self, group):
        self.group = group

    def __get_vector_by_spends_in_categories(self):
        to_ret = []
        for category in CATEGORIES:
            ids_of_shops_in_categories = self.df_shops.loc[self.df_shops['kategoria'] == category]['ID'].tolist()
            df_trans_user_in_cat = self.transactions_of_user[
                self.transactions_of_user['ID sklepu'].isin(ids_of_shops_in_categories)]
            amount_of_money = sum(df_trans_user_in_cat['kwota'].tolist())/30
            to_ret.append(amount_of_money)
        return to_ret

    def get_vector(self):
        to_return = [self.age, self.sex, self.salary]
        to_return += self.__get_vector_by_spends_in_categories()
        return to_return

    def how_often_this_category_per_month(self, category):
        ids_of_shops_in_categories = self.df_shops.loc[self.df_shops['kategoria'] == category]['ID'].tolist()

        dates_of_transactions_of_user = self.transactions_of_user['data'].tolist()
        max_date = datetime.strptime(max(dates_of_transactions_of_user), '%Y-%m-%d')
        min_date = datetime.strptime(min(dates_of_transactions_of_user), '%Y-%m-%d')
        delta = max_date - min_date
        months = delta.days/30

        number_of_transactions = len(self.transactions_of_user[
                                         self.transactions_of_user['ID sklepu'].isin(ids_of_shops_in_categories)])

        return number_of_transactions/months
