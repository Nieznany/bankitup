from flask import Flask, render_template, request
import pandas as pd
from bokeh.charts import Line, Area
from bokeh.embed import components
from bokeh.models import HoverTool, ColumnDataSource

app = Flask(__name__)

# Load the Data Set
df = pd.read_csv("../dane/user/media.csv", index_col = 0)
dates = pd.date_range('1/1/2015', '20/05/2017', freq = 'M')
dates = list(map(pd.to_datetime, dates))
dates = [x.strftime('%Y.%m') for x in dates]
types = df.type.unique()
df.date = pd.to_datetime(df.date)
df = df.set_index('date')
del df['amount']
# Create the main plot
def create_figure(date_start, date_end, typ):
    date_start = pd.to_datetime(date_start)
    date_end = pd.to_datetime(date_end)
    df1 = df[(df.index >= date_start) & (df.index <= date_end)]
    df1 = df1[df1.type == typ]
    source = ColumnDataSource(df1)
    p = Line(df1, y='price', x= 'index', color='type', legend='top_right', width=600, height=400, tools = 'hover', xlabel = 'Data')
    hover = p.select(dict(type = HoverTool))
    hover.tooltips = [("Typ", '@type'), ("Wartość", "$y")]
    p.yaxis.visible = False
	# Set the x axis label
	#p.xaxis.axis_label = current_feature_name
	# Set the y axis label
	#p.yaxis.axis_label = 'Count'
    return p

# Index page
@app.route('/')
def index():
	# Determine the selected feature
    date_start = request.args.get("date_start")
    date_start = pd.to_datetime(date_start)
    if date_start == None:
        date_start = dates[0]
    date_end = request.args.get("date_end")
    date_end = pd.to_datetime(date_end)
    if date_end == None:
        date_end = dates[-2]
    typ = request.args.get("typ")
    if typ == None:
        typ = types[0]
	# Create the plot
    plot = create_figure(date_start, date_end, typ)
	# Embed plot into HTML via Flask Render
    script, div = components(plot)
    return render_template("index_woda.html", script=script, div=div,dates=dates, date_start=date_start, date_end=date_end, types = types, typ = typ)

# With debug=True, Flask server will auto-reload
# when there are code changes
if __name__ == '__main__':
	app.run(port=5000, debug=True)
