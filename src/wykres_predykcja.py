from flask import Flask, render_template, request
import pandas as pd
from bokeh.charts import Line, Area
from bokeh.embed import components
from bokeh.models import HoverTool, ColumnDataSource
from bokeh.plotting import figure, output_file, show
from bokeh.models import DatetimeTickFormatter

app = Flask(__name__)

# Load the Data Set

cos = ['woda', 'gaz', 'czynsz', 'smieci', 'internet+tv', 'telefon', 'prad', 'fuel']
# Create the main plot
def create_figure(co):
    string = "../dane/user/estimation_"+co+".csv"
    df = pd.read_csv(string)
    val = df[df.date == '2017-05-21'].amount.values[0]
    ser = pd.Series({'date' : '2017-05-21', 'amount' : val, 'is_predicted' : 'real'})
    df = df.append(ser, ignore_index = True)
    df.date = pd.to_datetime(df.date)
    df = df.set_index('date')
    df = df.sort_index()
    #p = Line(df, y='amount', x="index", legend='bottom_right', color = "is_predicted", width=600, height=400, tools = 'hover', xlabel = 'Data')
    hover = HoverTool()
    hover.tooltips = [("Typ", '@is_predicted'), ("Wartość", "$y")]
    df1 = df[df.is_predicted == 'real']
    df2 = df[df.is_predicted == 'estimated']
    source1 = ColumnDataSource(df1)
    source2 = ColumnDataSource(df2)
    p = figure(width=600, height=400, tools = [hover],toolbar_location=None, title="Predykcja wydatku")
    p.line(x = 'date', y = 'amount', source = source1, line_width = 7, line_color = 'blue', legend = "Dane historyczne")
    p.line(x = 'date', y = 'amount', source = source2, line_width = 7, line_color = 'red', legend = 'Prognoza')
    p.xaxis.formatter=DatetimeTickFormatter(
        hours=["%d %B %Y"],
        days=["%d %B %Y"],
        months=["%d %B %Y"],
        years=["%d %B %Y"],
    )
    p.xaxis.major_label_orientation = -3.14/4
    p.yaxis.visible = False
    p.legend.location = 'bottom_right'
    return p

# Index page
@app.route('/')
def index():
	# Determine the selected feature
    co = request.args.get("co")
    if co == None:
        co = 'czynsz'

	# Create the plot
    plot = create_figure(co)

	# Embed plot into HTML via Flask Render
    script, div = components(plot)
    return render_template("index_predykcja.html", script=script, div=div, co = co, cos = ['internet+tv', 'telefon'])

# With debug=True, Flask server will auto-reload
# when there are code changes
if __name__ == '__main__':
	app.run(port=5000, debug=True)
