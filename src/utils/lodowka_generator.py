import pandas as pd
import numpy as np
def lodowka_gen(n):
    month = pd.date_range('1/1/2015', '20/05/2017', freq = 'D')
    cena = np.random.normal(n, (n/10), len(month))
    df = pd.DataFrame({'date' : month, 'amount' : cena})
    df.to_csv('../../dane/user/lodowka.csv')

if __name__ == '__main__':
    n = int(input("Wpisz średnie wydatki na jedzenie w dzień: "))
    lodowka_gen(n)
