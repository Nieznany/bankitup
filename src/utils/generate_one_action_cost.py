import pandas as pd
import pickle
import logging
import os

import dane.globalny.classes as classes
#from src.utils.count_cost_iot import media_estimation
from dane.globalny.config import list_of_categories
import src.utils.count_cost_iot as count_cost_iot

USER_CSV = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))),
                        'dane', 'user', 'user.csv')
OUTPUT_DIR = os.path.dirname(USER_CSV)
SAVINGS_CSV = os.path.join(os.path.dirname(OUTPUT_DIR), 'globalny', 'cele.csv')


def get_my_savings_planned():
    if os.path.exists(SAVINGS_CSV):
        with open(SAVINGS_CSV, 'r') as fp:
            for line in fp:
                value = line
                break
        try:
            return float(value)
        except Exception as e:
            logging.warning('{}'.format(e))
            return 0
    logging.info('No savings found!')
    return 0

if __name__ == '__main__':
    count_cost_iot.main()

    with open(count_cost_iot.PICKLE_NAME, 'rb') as fp:
        media_estimation = pickle.load(fp)

    income = pd.read_csv(USER_CSV)['kwota'].tolist()[0]
    outcome = media_estimation.get_total_estimated_cost()
    my_saving_plan = get_my_savings_planned()
    money_left = income - outcome - my_saving_plan

    if money_left <= 0:
        money_left = income - outcome

    user_id = pd.read_csv(USER_CSV)['ID'].tolist()[0]

    obj = classes.Customer(user_id)

    output_df = pd.DataFrame(columns=['category', 'how_often_in_month', 'money'])

    for index, category in enumerate(list_of_categories):
        vector_of_categories = obj.get_vector()[3:]
        #print(vector_of_categories)
        output_df.loc[len(output_df)] = [category, obj.how_often_this_category_per_month(category),
                                         money_left*vector_of_categories[index]/(sum(vector_of_categories) *
                                                                                 obj.how_often_this_category_per_month(category))]

    output_df.to_csv(os.path.join(OUTPUT_DIR, 'one_go_this_month.csv'))