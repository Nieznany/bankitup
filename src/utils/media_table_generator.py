import pandas as pd
import numpy as np

def media_gen(osoba):
    if osoba == "bogaty": n = 5
    elif osoba == "biedny": n = -5
    else: n = 2000
    month = pd.date_range('1/1/2015', '20/05/2017', freq = 'D')
    month = np.repeat(month, 7)
    typ = ['woda', 'gaz', 'prad', 'czynsz',
            'smieci', 'internet+tv', 'telefon'] * int(((len(month))/7))
    woda = np.random.normal(88/30 + 88/(n*30),2/5,size = int(((len(month))/7)))
    gaz = np.random.normal(128/30 + 128/(n*30),3/5,int(((len(month))/7)))
    prad = np.random.normal(176/30 + 176/(n*30),3/5,int(((len(month))/7)))
    czynsz = np.random.normal(460/30 + 460/(n*30),4/5,int(((len(month))/7)))
    smieci = np.random.normal(40/30 + 40/(n*30),2/5,int(((len(month))/7)))
    it = np.random.normal(100/30 + 100/(n*30),2/5,int(((len(month))/7)))
    telefon = np.random.normal(100/30 + 100/(n*30),2/5,int(((len(month))/7)))

    woda1 = np.cumsum(woda)
    gaz1 = np.cumsum(gaz)
    prad1 = np.cumsum(prad)
    czynsz1 = np.cumsum(czynsz)
    smieci1 = np.cumsum(smieci)
    it1 = np.cumsum(it)
    telefon1 = np.cumsum(telefon)
    m = []
    for i1,i2,i3,i4,i5,i6,i7 in zip(woda,gaz,prad,czynsz,smieci,it,telefon):
        m.append(round(i1,2))
        m.append(round(i2,2))
        m.append(round(i3,2))
        m.append(round(i4,2))
        m.append(round(i5,2))
        m.append(round(i6,2))
        m.append(round(i7,2))
    m1 = []
    for i1,i2,i3,i4,i5,i6,i7 in zip(woda1,gaz1,prad1,czynsz1,smieci1,it1,telefon1):
        m1.append(round(i1,2))
        m1.append(round(i2,2))
        m1.append(round(i3,2))
        m1.append(round(i4,2))
        m1.append(round(i5,2))
        m1.append(round(i6,2))
        m1.append(round(i7,2))
    df = pd.DataFrame({'date' : month, 'type' : typ, 'amount' : m1, 'price' : m})
    df.to_csv('../../dane/user/media.csv')

if __name__ == '__main__':
    n = input("Jaki jest user biedny, sredni, bogaty? \n")
    media_gen(n)
