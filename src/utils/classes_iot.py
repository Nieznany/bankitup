import pandas as pd
import numpy as np
import logging
import datetime
import time


class Media:
    def __init__(self, path_to_csv_file, what_media_is_it=None, today_date=datetime.date.today()):
        self.col_name_date = 'date'
        self.col_name_amoun = 'amount'
        self.col_name_what_it_is = 'type'

        self.what_it_is = what_media_is_it
        self.df = pd.read_csv(path_to_csv_file)
        if self.what_it_is is not None:
            self.df = self.df.loc[self.df[self.col_name_what_it_is] == self.what_it_is]

        #pd.to_datetime(self.df[self.col_name_date], format='%Y-%m-%d')
        self.day_of_month_of_payment = 1
        self.today_date = today_date

    def change_day_of_payment(self, day):
        """
        
        :param day: int or float
        :return: 
        """
        try:
            self.day_of_month_of_payment = int(day)
        except ValueError:
            logging.error('Bad format of day number!')
            pass

    def __find_next_day_with_day_nbr(self, day):
        flag = False
        temp_date = self.today_date
        number_of_days_later = 0
        while flag is False:
            if temp_date.day == day:
                return [temp_date, number_of_days_later]
            else:
                temp_date = temp_date + datetime.timedelta(days=1)
                number_of_days_later += 1

    def __estimate_amount_on_day(self, day):
        # datetime.datetime(2016, 3, 9, 0, 0)def convertdate(dstring):
        #     return datetime.datetime.strptime(dstring, '%Y-%m-%d')

        day_proper_format = self.__find_next_day_with_day_nbr(day)[0]
        how_many_days_must_be_gone = self.__find_next_day_with_day_nbr(day)[1]

        dates = [item for item in self.df[self.col_name_date].tolist()]
        values = self.df[self.col_name_amoun].tolist()

        dates_indexes = [index for index, _ in enumerate(dates)]

        # print(dates)
        # print(self.today_date.strftime('%Y-%m-%d'))
        today_date_index = dates.index(self.today_date.strftime('%Y-%m-%d'))

        desired_date_index = max(dates_indexes) + how_many_days_must_be_gone

        x = np.array(dates_indexes)
        y = np.array(values)
        z = np.polyfit(x, y, 4)

        p4 = np.poly1d(z)

        predicted_value_on_desired_date = p4(desired_date_index)

        return predicted_value_on_desired_date

    def __estimate_amount_on_payday(self):
        est_value = self.__estimate_amount_on_day(self.day_of_month_of_payment)
        return est_value

    def get_amount_at_day(self, day):
        str_day = datetime.datetime.now().strftime('%Y-%m-')+'%02d'%day
        dates = self.df[self.col_name_date].tolist()
        if str_day in dates:
            return [self.df.loc[self.df[self.col_name_date] == str_day][self.col_name_amoun].tolist()[0], 'real']
        else:
            return [self.__estimate_amount_on_day(day), 'estimated']

    def count_difference_this_month(self):
        end_month_value = self.__estimate_amount_on_payday()
        begin_day_month = self.__find_next_day_with_day_nbr(self.day_of_month_of_payment)[0] - \
                          datetime.timedelta(days=30)
        try:
            begin_month_value = self.df.loc[self.df[self.col_name_date] == begin_day_month.strftime('%Y-%m-%d')][self.col_name_amoun].tolist()[0]
        except IndexError:
            logging.critical('Not find today date! fatal')
            begin_month_value = end_month_value - 10

        return abs(end_month_value - begin_month_value)
