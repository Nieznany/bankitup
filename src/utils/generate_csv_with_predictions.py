import datetime
import os
import pandas as pd
import numpy as np
from calendar import monthrange

from src.utils.classes_iot import Media
from src.utils.count_cost_iot import CSV_FILE_WITH_AGGR_VALUES_MEDIA
from src.utils.count_cost_iot import CSV_FILE_WITH_AGGR_VALUES_CAR
from src.utils.count_cost_iot import MEDIA

CSV_DEST_FOLDER = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath('.'))), 'dane', 'user')


def create_and_save_csv(obj, media):
    df = pd.DataFrame(columns=['date', 'amount', 'is_predicted'])

    for day_minus_one in range(monthrange(datetime.datetime.now().year, datetime.datetime.now().month)[1]):
        day = day_minus_one + 1
        value = obj.get_amount_at_day(day)

        df.loc[len(df)] = [datetime.datetime.now().strftime('%Y-%m-') + str(day), value[0], value[1]]
        df.to_csv(os.path.join(CSV_DEST_FOLDER, 'estimation_{}.csv'.format(media)), index=False)


def main():
    for media in MEDIA:
        obj = Media(CSV_FILE_WITH_AGGR_VALUES_MEDIA, media)
        create_and_save_csv(obj, media)
    obj = Media(CSV_FILE_WITH_AGGR_VALUES_CAR)
    create_and_save_csv(obj, 'fuel')


if __name__ == '__main__':
    main()
