import os
import pickle

from src.utils.classes_iot import Media

CSV_FILE_WITH_AGGR_VALUES_MEDIA = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath('.'))), 'dane',
                                         'user', 'media.csv')
CSV_FILE_WITH_AGGR_VALUES_CAR = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath('.'))), 'dane',
                                         'user', 'fuel.csv')
MEDIA = ['woda', 'gaz', 'prad', 'czynsz', 'smieci', 'internet+tv', 'telefon']

PICKLE_NAME = 'media_estimation.pickle'


class OneMediaCostEstimation:
    def __init__(self, csv_path, media_name=None, date_today=None):
        self.obj_with_est = Media(path_to_csv_file=csv_path, what_media_is_it=media_name)

    def estimate_cost_this_month(self):
        return self.obj_with_est.count_difference_this_month()


class FullMediaCostEstimation:
    def __init__(self):
        self.dictionary_with_usages = {}

    def add_object(self, obj, label):
        self.dictionary_with_usages[label] = obj.estimate_cost_this_month()

    def get_total_estimated_cost(self):
        return sum(self.dictionary_with_usages.values())

    def get_est_cost_by_label(self, label):
        return self.dictionary_with_usages[label]


def main():
    media_estimation = FullMediaCostEstimation()
    for media in MEDIA:
        one_media_obj = OneMediaCostEstimation(CSV_FILE_WITH_AGGR_VALUES_MEDIA, media)
        media_estimation.add_object(obj=one_media_obj, label=media)

    fuel_obj = OneMediaCostEstimation(CSV_FILE_WITH_AGGR_VALUES_CAR)
    media_estimation.add_object(fuel_obj, 'fuel')
    with open(PICKLE_NAME, 'wb') as fp:
        pickle.dump(media_estimation, fp)

if __name__ == '__main__':
    main()

