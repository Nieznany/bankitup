from flask import Flask, render_template, request
import pandas as pd
from bokeh.charts import Bar
from bokeh.embed import components
from bokeh.models import HoverTool, ColumnDataSource
from bokeh.models import Select
from bokeh.layouts import row, widgetbox
app = Flask(__name__)

# Load the Data Set
df = pd.read_csv("../dane/user/media.csv", index_col = 0)
dates = pd.date_range('1/1/2015', '20/05/2017', freq = 'M')
dates = list(map(pd.to_datetime, dates))
dates = [x.strftime('%Y.%m') for x in dates]
df.date = pd.to_datetime(df.date)
df = df.set_index('date')
del df['amount']
# g = df.groupby([pd.TimeGrouper('M'), df.type])
# df1 = g.sum()

# Create the main plot
def create_figure(date):
    date = pd.to_datetime(date)
    df1 = df[(df.index.month == date.month) & (df.index.year == date.year)]
    source = ColumnDataSource(df1)
    p = Bar(df1, values='price', label="type", color='type', agg='sum', legend='top_right', width=600, height=400, tools = 'hover')
    hover = p.select(dict(type = HoverTool))
    hover.tooltips = [("Typ", '@x'), ('Wartość', '@height')]
    p.xaxis.visible = False
    p.yaxis.visible = False
	# Set the x axis label
	#p.xaxis.axis_label = current_feature_name
	# Set the y axis label
	#p.yaxis.axis_label = 'Count'
    return p
# Index page
@app.route('/')
def index():
	# Determine the selected feature
    date = request.args.get("date")
    date = pd.to_datetime(date)
    if date == None:
        date = dates[-1]

	# Create the plot
    plot = create_figure(date)

	# Embed plot into HTML via Flask Render
    script, div = components(plot)
    return render_template("iris_index1.html", script=script, div=div,dates=dates,  date=date)

# With debug=True, Flask server will auto-reload
# when there are code changes
if __name__ == '__main__':
	app.run(port=5000, debug=True)
