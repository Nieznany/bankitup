#!/usr/bin/env python
from flask import Flask, render_template, session, request
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect, send
from random import randint
from bokeh.charts import Bar, Line
from bokeh.embed import components
from bokeh.models import HoverTool, ColumnDataSource
from bokeh.models import Select
from bokeh.layouts import row, widgetbox
import os
import pandas as pd
from bokeh.plotting import figure, output_file, show
from bokeh.models import DatetimeTickFormatter
cos = ['woda', 'gaz', 'czynsz', 'smieci', 'internet+tv', 'telefon', 'prad', 'fuel']

# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = None

app = Flask(__name__)
df = pd.read_csv("dane/user/media.csv", index_col = 0)
dates = pd.date_range('1/1/2015', '20/05/2017', freq = 'M')
dates = list(map(pd.to_datetime, dates))
dates = [x.strftime('%Y.%m') for x in dates]
df.date = pd.to_datetime(df.date)
df = df.set_index('date')
types = df.type.unique()
del df['amount']
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
log = []
def create_sec_figure(date_start, date_end, typ):
    date_start = pd.to_datetime(date_start)
    date_end = pd.to_datetime(date_end)
    df1 = df[(df.index >= date_start) & (df.index <= date_end)]
    df1 = df1[df1.type == typ]
    source = ColumnDataSource(df1)
    p = Line(df1, y='price', x= 'index', color='type', legend='top_right', width=1000, height=400, tools = 'hover', xlabel = 'Data')
    hover = p.select(dict(type = HoverTool))
    hover.tooltips = [("Typ", '@type'), ("Wartość", "$y")]
	# Set the x axis label
	#p.xaxis.axis_label = current_feature_name
	# Set the y axis label
	#p.yaxis.axis_label = 'Count'
    return p
def create_figure(date):
    date = pd.to_datetime(date)
    df1 = df[(df.index.month == date.month) & (df.index.year == date.year)]
    source = ColumnDataSource(df1)
    p = Bar(df1, values='price', label="type", color='type', agg='sum', legend='top_right', width=1000, height=400, tools = 'hover')
    hover = p.select(dict(type = HoverTool))
    hover.tooltips = [("Typ", '@x'), ('Wartość', '@height')]
    p.xaxis.visible = False
	# Set the x axis label
	#p.xaxis.axis_label = current_feature_name
	# Set the y axis label
	#p.yaxis.axis_label = 'Count'
    return p
def create_pred(co):
    string = "dane/user/estimation_"+co+".csv"
    df = pd.read_csv(string)
    val = df[df.date == '2017-05-21'].amount.values[0]
    ser = pd.Series({'date' : '2017-05-21', 'amount' : val, 'is_predicted' : 'real'})
    df = df.append(ser, ignore_index = True)
    df.date = pd.to_datetime(df.date)
    df = df.set_index('date')
    df = df.sort_index()
    #p = Line(df, y='amount', x="index", legend='bottom_right', color = "is_predicted", width=600, height=400, tools = 'hover', xlabel = 'Data')
    hover = HoverTool()
    hover.tooltips = [("Typ", '@is_predicted'), ("Wartość", "$y")]
    df1 = df[df.is_predicted == 'real']
    df2 = df[df.is_predicted == 'estimated']
    source1 = ColumnDataSource(df1)
    source2 = ColumnDataSource(df2)
    p = figure(width=1000, height=600, tools = [hover],toolbar_location=None, title="Predykcja wydatku")
    p.line(x = 'date', y = 'amount', source = source1, line_width = 7, line_color = 'blue', legend = "Dane historyczne")
    p.line(x = 'date', y = 'amount', source = source2, line_width = 7, line_color = 'red', legend = 'Prognoza')
    p.xaxis.formatter=DatetimeTickFormatter(
        hours=["%d %B %Y"],
        days=["%d %B %Y"],
        months=["%d %B %Y"],
        years=["%d %B %Y"],
    )
    p.xaxis.major_label_orientation = -3.14/4
    p.legend.location = 'bottom_right'
    return p
@app.route('/')
def index():
    date_start = request.args.get("date_start")
    date_start = pd.to_datetime(date_start)
    if date_start == None:
        date_start = dates[0]
    date_end = request.args.get("date_end")
    date_end = pd.to_datetime(date_end)
    if date_end == None:
        date_end = dates[-2]
    typ = request.args.get("typ")
    if typ == None:
        typ = types[0]
	# Create the plot
    plot1 = create_sec_figure(date_start, date_end, typ)
	# Embed plot into HTML via Flask Render
    script1, div1 = components(plot1)





    date = request.args.get("date")
    date = pd.to_datetime(date)
    if date == None:
        date = dates[-1]

	# Create the plot
    plot = create_figure(date)

	# Embed plot into HTML via Flask Render
    script, div = components(plot)
    return render_template('index.html', async_mode=socketio.async_mode,script=script, div=div,dates=dates, date=date,script1=script1, div1=div1, types=types)

@app.route('/life')
def life():
    return render_template('life.html', async_mode=socketio.async_mode)
@app.route('/life/gastro')
def gas():
    return render_template('gastro.html', async_mode=socketio.async_mode)

@app.route('/life/enter')
def ent():
    return render_template('enter.html', async_mode=socketio.async_mode)

@app.route('/life/odziez')
def odz():
    return render_template('odziez.html', async_mode=socketio.async_mode)

@app.route('/life/shops')
def shop():
    return render_template('shops.html', async_mode=socketio.async_mode)

@app.route('/life/cele')
def cele():
    return render_template('cele.html', async_mode=socketio.async_mode)

@app.route('/menager')
def menager():
    return render_template('menager.html', async_mode=socketio.async_mode)

@app.route('/menager/gaz')
def gaz():
    co = 'gaz'

	# Create the plot
    plot = create_pred(co)

	# Embed plot into HTML via Flask Render
    script, div = components(plot)
    return render_template('gaz.html', async_mode=socketio.async_mode, script=script, div=div, co = co, cos = cos)

@app.route('/menager/woda')
def woda():
    co = 'woda'

	# Create the plot
    plot = create_pred(co)

	# Embed plot into HTML via Flask Render
    script, div = components(plot)
    return render_template('woda.html', async_mode=socketio.async_mode, script=script, div=div, co = co, cos = cos)

@app.route('/menager/prad')
def prad():
    co = 'prad'

	# Create the plot
    plot = create_pred(co)

	# Embed plot into HTML via Flask Render
    script, div = components(plot)
    return render_template('prad.html', async_mode=socketio.async_mode, script=script, div=div, co = co, cos = cos)

@app.route('/menager/car')
def car():
    co = 'fuel'

	# Create the plot
    plot = create_pred(co)

	# Embed plot into HTML via Flask Render
    script, div = components(plot)
    return render_template('car.html', async_mode=socketio.async_mode, script=script, div=div, co = co, cos = cos)

@app.route('/menager/media')
def media():
    co = 'internet+tv'
    plot = create_pred(co)

	# Embed plot into HTML via Flask Render
    script, div = components(plot)
	# Create the plot
    co1 = 'telefon'
    plot2 = create_pred(co)

	# Embed plot into HTML via Flask Render
    script2, div2 = components(plot2)
    return render_template('media.html', async_mode=socketio.async_mode, script=script, div=div,script1=script2, div1=div2, co = co, cos = ['internet+tv', 'telefon'])
@app.route('/menager/budget')
def budget():
    return render_template('budget.html', async_mode=socketio.async_mode)

@socketio.on('connect', namespace = '/life/gastro')
def test_connect_gas():
    csv_op = pd.read_csv('dane/globalny/gastronomia.csv')
    csv_mon = pd.read_csv('dane/user/one_go_this_month.csv')
    first_id = int(csv_op['0'][0])
    second_id=int(csv_op['0'][1])
    third_id=int(csv_op['0'][2])
    csv_op = pd.read_csv('dane/user/opinie.csv')
    first_ocena = csv_op['ocena'][1]
    print(first_ocena)
    second_ocena = csv_op['ocena'][second_id]
    third_ocena = csv_op['ocena'][third_id]
    first_opinia = csv_op['opinia'][first_id]
    second_opinia = csv_op['opinia'][second_id]
    third_opinia = csv_op['opinia'][third_id]
    emit('dane', {'often': round(csv_mon['how_often_in_month'][0]),'money': round(csv_mon['money'][0]),'first': {'id': first_id, 'ocena': int(first_ocena), 'opinia': first_opinia},'second': {'id': second_id, 'ocena': int(second_ocena), 'opinia': second_opinia}, 'third': {'id': third_id, 'ocena': int(third_ocena), 'opinia': third_opinia}})

@socketio.on('connect', namespace = '/')
def test_con12nect():
    pass
@socketio.on('connect', namespace = '/menager/gaz')
def test_connect():
    print("Wysylam")
    emit('dane', {'stan': gaz_stan, 'predictedstan': 500, 'predictedcosts': 1200, 'costs': 500})
    print("Wysylam")
@socketio.on('cel')
def test_dvee(msg):
    f = open('dane/globalny/cele.csv', 'w')
    print(msg)
    f.write(msg)

@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    log.remove(request.sid)
    print('Client disconnected', request.sid)


if __name__ == '__main__':
    csv_op = pd.read_csv('dane/globalny/gastronomia.csv')
    first_id = csv_op['0'][0]
    print(first_id)
    socketio.run(app, debug=True)
